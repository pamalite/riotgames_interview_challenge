package com.riotgames.interview.hongkong.matchmaking;

import junit.framework.TestCase;

import java.util.Random;
import java.util.List;
import java.util.ArrayList;

public class MatchmakerImplTest extends TestCase {
    
    Matchmaker matchmakerFair;
    Matchmaker matchmakerStrong;
    Matchmaker matchmakerWeak;
    List<Player> soloPlayers;
    List<Player> strongSoloPlayers;
    List<Player> weakSoloPlayers;

    private void generatePlayers() {
        // generate 200 players
        this.soloPlayers = new ArrayList<Player>();
        this.strongSoloPlayers = new ArrayList<Player>();
        this.weakSoloPlayers = new ArrayList<Player>();

        for (int i=0; i < 50; i++) {
            String playerName = String.format("Player %d", i);

            Random rand = new Random();

            // normal
            long wins = (long) rand.nextInt(1000) + 1;
            long losses = (long) rand.nextInt(1000) + 1;

            this.soloPlayers.add(new Player(playerName, wins, losses));

            // strong
            wins = (long) rand.nextInt(1000) + 1;
            losses = (long) rand.nextInt(100) + 1;

            this.strongSoloPlayers.add(new Player(playerName, wins, losses));

            // weak
            wins = (long) rand.nextInt(100) + 1;
            losses = (long) rand.nextInt(1000) + 1;

            this.weakSoloPlayers.add(new Player(playerName, wins, losses));
        }
    }

    public void setUp() {
        // mock up some players
        this.generatePlayers();

        // instantiate default matchmakers
        this.matchmakerFair = new MatchmakerImpl(MatchmakerMode.FAIR);
        this.matchmakerStrong = new MatchmakerImpl(MatchmakerMode.STRONG);
        this.matchmakerWeak = new MatchmakerImpl(MatchmakerMode.WEAK);
    }

    public void testStrongDefaultBias() {
        assertEquals(((MatchmakerImpl) this.matchmakerStrong).getBias(), 0.7);
    }

    public void testWeakDefaultBias() {
        assertEquals(((MatchmakerImpl) this.matchmakerWeak).getBias(), 0.7);
    }

    public void testFairMode() {
        assertEquals(((MatchmakerImpl) this.matchmakerFair).getMode(), MatchmakerMode.FAIR);
    }

    public void testStrongMode() {
        assertEquals(((MatchmakerImpl) this.matchmakerStrong).getMode(), MatchmakerMode.STRONG);
    }

    public void testWeakMode() {
        assertEquals(((MatchmakerImpl) this.matchmakerWeak).getMode(), MatchmakerMode.WEAK);
    }

    public void testFairMatchingPlayersCountBalanced() {
        // add all players for matchmaking
        for (Player player : this.soloPlayers) {
            this.matchmakerFair.enterMatchmaking(player);
        }

        // get a new match
        Match match = this.matchmakerFair.findMatch(3);

        assertTrue(match.getTeam1().size() == match.getTeam2().size());
    }

    public void testStrongMatchingPlayersHaveMoreWins() {
        // add all players for matchmaking
        for (Player player : this.strongSoloPlayers) {
            this.matchmakerStrong.enterMatchmaking(player);
        }

        // get a new match
        int playersPerTeam = 3;
        Match match = this.matchmakerStrong.findMatch(playersPerTeam);

        // combine both teams
        List<Player> players = new ArrayList<Player>(match.getTeam1());
        players.addAll(match.getTeam2());

        int weakPlayersCount = 0;
        int strongPlayersCount = 0;
        for (Player player : players) {
            if (player.getWins() < player.getLosses()) {
                weakPlayersCount++;
            } else {
                strongPlayersCount++;
            }
        }

        assertTrue(weakPlayersCount <= 0 || weakPlayersCount < strongPlayersCount);
    }

    public void testWeakMatchingPlayersHaveMoreLosses() {
        // add all players for matchmaking
        for (Player player : this.weakSoloPlayers) {
            this.matchmakerStrong.enterMatchmaking(player);
        }

        // get a new match
        int playersPerTeam = 3;
        Match match = this.matchmakerStrong.findMatch(playersPerTeam);

        // combine both teams
        List<Player> players = new ArrayList<Player>(match.getTeam1());
        players.addAll(match.getTeam2());

        int strongPlayersCount = 0;
        int weakPlayersCount = 0;
        for (Player player : players) {
            if (player.getWins() > player.getLosses()) {
                strongPlayersCount++;
            } else {
                weakPlayersCount++;
            }
        }

        assertTrue(strongPlayersCount <= 0 || strongPlayersCount < weakPlayersCount);
    }
}
