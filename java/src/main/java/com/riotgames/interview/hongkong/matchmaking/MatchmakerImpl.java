package com.riotgames.interview.hongkong.matchmaking;

import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Random;
import java.util.HashSet;

/**
 *  This is a simple Matchmaker implemetation that uses the simple Quartile statistics based on player's losses count.<br><br>
 * 
 * To use this class, the Matchmaker must be instantiate with a {@link com.riotgames.interview.hongkong.matchmaking.MatchmakerMode MatchmakerMode}.<br><br>
 * 
 * The modes are:
 * <ul>
 *  <li>FAIR: Randomly pick each player from each quartile until players per team is satisfied.</li>
 *  <li>STRONG: Same as FAIR, but favors players with more wins.</li>
 *  <li>WEAK: Same as FAIR, but favors players with more losses.</li>
 * </ul>
 * <br>
 * 
 * When players are entered for matchmaking, it will first sort the players in descending order based on their losses. <br><br>
 * 
 * Example usage: <br><br>
 * <pre>
 * {@code Matchmaker matchmaker = new MatchmakerImpl(MatchmakerMode.FAIR);
 * 
 * for (Player player : listOfPlayers) {
 *    matchmaker.enterMatchmaking(player);
 * }
 * 
 * int playersPerTeam = 3;
 * Match match = matchmaker.findMatch(playersPerTeam);
 * 
 * List<Player> team1 = new ArrayList<Player>(match.getTeam1());
 * List<Player> team2 = new ArrayList<Player>(match.getTeam2());
 * }
 * </pre>
 * 
 * @author Ken S'ng Wong
 */
public class MatchmakerImpl implements Matchmaker {

    private final MatchmakerMode mode;

    private double bias;
    private List<Player> soloPlayersToMatch;
    private List<Player> playersToRemove;

    /**
     * The constructor for this Matchmaker. It defaults mode with bias of 0.7. 
     * <p>
     * The bias is not used when mode is FAIR.
     * </p>
     * @param mode Can be either FAIR, STRONG or WEAK.
     */
    public MatchmakerImpl(MatchmakerMode mode) {
        this.mode = mode;
        
        // initialize lists
        this.soloPlayersToMatch = new ArrayList<Player>();
        this.playersToRemove = new ArrayList<Player>();

        // defaults bias to 70% if mode is not FAIR
        this.bias = 0.7;
    }

    /**
     * Get the current mode this Matchmaker is operating in.
     * @return MatchmakerMode
     */
    public MatchmakerMode getMode() {
        return this.mode;
    }

    /**
     * Set the bias for the Matchmaker to behave.
     * @param newBias Range of 0.0 to 1.0. 
     */
    public void setBias(double newBias) {
        this.bias = (newBias > 0 && newBias <= 1.0) ? newBias : this.bias;
    }

    /**
     * Get the current bias the Matchmaker is behaving with.
     * @return double
     */
    public double getBias() {
        return this.bias;
    }

    /**
     * Get the refreshed list of quartiles from the current players.
     * @return List<List<Player>> List of quartiles, each a List of Player's.
     */
    private List<List<Player>> getQuartiles() {
        List<List<Player>> quartiles = new ArrayList<List<Player>>();
        List<Player> quartile = new ArrayList<Player>();

        int currentQuartileIndex = 0;
        int playerIndex = 0;
        for (Player player : this.soloPlayersToMatch) {
            quartile.add(player);
            playerIndex = playerIndex + 1;

            double whichQuartile = (double) playerIndex / (double) this.soloPlayersToMatch.size();
            int quartileIndex;
            if (whichQuartile <= 0.25) {
                quartileIndex = 0;
            } else if (whichQuartile > 0.25 && whichQuartile <= 0.5) {
                quartileIndex = 1;
            } else if (whichQuartile > 0.5 && whichQuartile <= 0.75) {
                quartileIndex = 2;
            } else {
                quartileIndex = 3;
            }

            if (quartileIndex != currentQuartileIndex) {
                quartiles.add(quartile);
                quartile.clear();
                currentQuartileIndex = quartileIndex;
            }
        }

        // last quartile to be added
        quartiles.add(quartile);
        
        return quartiles;
    }

    /**
     * The core method that forms a team based on the mode, bias and playersPerTeam set. 
     * @param playersPerTeam
     * @return List<Player> A List of players, or {@code null}.
     */
    private List<Player> formTeamOf(int playersPerTeam) {
        
        List<List<Player>> quartiles = this.getQuartiles();
        if (this.mode == MatchmakerMode.STRONG) {
            // reverse the quartiles if the formation favors stronger players
            Collections.reverse(quartiles);
        }

        // set bias following mode
        int playersPerQuartile = 1;
        if (this.mode != MatchmakerMode.FAIR) {
            // bias to more players with higher wins (STRONG) or losses (WEAK)
            playersPerQuartile = (int) Math.round((double) playersPerTeam * this.bias);
        }

        List<Player> outTeam = new ArrayList<Player>();
        Random rand = new Random();
        int startIndex = 0;

        int quartileIndex = 0;
        int playerBiasCount = 0;
        while (outTeam.size() < playersPerTeam) {
            List<Player> quartile = quartiles.get(quartileIndex);
            int endIndex = quartile.size() - 1;
            
            // picked player(s) from quartile; make sure is not already picked
            Player pickedPlayer = null;
            int pickingCount = 0;
            do {
                int pickedIndex = rand.nextInt(endIndex - startIndex) + startIndex;
                pickedPlayer = quartile.get(pickedIndex);
                pickingCount++;

                // exhausted?
                if (pickingCount >= quartile.size()) {
                    // yes
                    break;
                }

                // check the wins and losses based on mode bias
                if (this.mode == MatchmakerMode.STRONG) {
                    // wins >= losses
                    if (pickedPlayer.getWins() < pickedPlayer.getLosses()) {
                        // try again
                        pickedPlayer = null;
                        continue;
                    }
                } else if (this.mode == MatchmakerMode.WEAK) {
                    // wins <= losses
                    if (pickedPlayer.getWins() > pickedPlayer.getLosses()) {
                        // try again
                        pickedPlayer = null;
                        continue;
                    }
                }
            } while (
                pickedPlayer == null || 
                this.playersToRemove.contains(pickedPlayer) || 
                outTeam.contains(pickedPlayer)
            );

            // add to team
            if (pickedPlayer != null) {
                outTeam.add(pickedPlayer);
                
                // add picked player to placeholder
                this.playersToRemove.add(pickedPlayer);

                // next quartile or stay?
                playerBiasCount = playerBiasCount + 1;
                if (playerBiasCount < playersPerQuartile) {
                    // stay in current quartile
                    continue;
                }
                playerBiasCount = 0;
            }

            // increment quartile index to get next quartile
            quartileIndex = (quartileIndex + 1) % 4;
        }
        
        return (outTeam.isEmpty()) ? null : outTeam;
    }
    
    /**
     * Find matching players based on playersPerTeam, mode and bias set.
     * 
     * Matched players are purged from the database in this class. 
     * To have matched players re-matched, they must be added back with {@link com.riotgames.interview.hongkong.matchmaking.Matchmaker#enterMatchmaking(Player) Matchmaker.enterMatchmaking()}.
     * 
     * @param playersPerTeam The number of players a team.
     * @return Match The matching teams, or {@code null}.
     */
    public Match findMatch(int playersPerTeam) {
        // if no players, then stop
        if (this.soloPlayersToMatch.size() <= 0) {
            return null;
        }

        // make sure playersPerTeam is > 0 and <= 50% all players
        if (playersPerTeam <= 0 || playersPerTeam >= (this.soloPlayersToMatch.size() / 2)) {
            // cannot have <= 0 or >= 50% players count
            System.err.printf("<players_per_team> MUST be > 0 or <= %d\n", (this.soloPlayersToMatch.size() / 2));
            return null;
        }

        // sort players by losses to give the underdogs better chances
        this.soloPlayersToMatch.sort(new Comparator<Player>() {
            public int compare(Player aPlayer, Player bPlayer) {
                if (aPlayer.getLosses() > bPlayer.getLosses()) {
                    return -1;
                }
                return (aPlayer.getLosses() < bPlayer.getLosses()) ? 1 : 0;
            }
        });

        // try to return a balanced match
        List<Player> team1 = null;
        List<Player> team2 = null;
        int triesCount = 5;
        do {
            team1 = this.formTeamOf(playersPerTeam);
            team2 = this.formTeamOf(playersPerTeam);

            triesCount--;
            if (triesCount <= 0) {
                System.err.println("Unable to find suitable balanced match from data.");
                return null;
            }
        } while (
            team1 != null && team2 != null &&
            team1.size() != team2.size()
        );
        
        // remove players from database
        if (!this.playersToRemove.isEmpty()) {
            this.soloPlayersToMatch.removeAll(this.playersToRemove);
            this.playersToRemove.clear();
        }

        return (team1 == null || team2 == null) ? null : new Match(
            new HashSet<Player>(team1), 
            new HashSet<Player>(team2)
        );
    }

    /**
     * Add a waiting player for matching. 
     * @param player A player to be matched.
     */
    public void enterMatchmaking(Player player) {
        // is the player already in the list?
        if (this.soloPlayersToMatch.contains(player)) {
            // no longer able to enter for matching
            return;
        }

        this.soloPlayersToMatch.add(player);
    }

}
