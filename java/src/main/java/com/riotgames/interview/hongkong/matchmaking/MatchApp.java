package com.riotgames.interview.hongkong.matchmaking;

import java.io.FileReader;
import java.io.IOException;

import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * This is a test/example app used to demonstrate how the Matchmaker with MatchmakerImpl is ued. <br><br>
 * 
 * To run this app:<br><br>
 * <pre>
 * {@code > java -cp .:/path/to/json-simple-1.1.1.jar MatchApp /path/to/players_data.json playersPerTeam}
 * </pre>
 * or<br><br>
 * <pre>
 * {@code > java -jar hongkong-0.0.1-SNAPSHOT-jar-with-dependencies.jar /path/to/players_data.json playersPerTeam}
 * </pre>
 * @author Ken S'ng Wong
 */
public class MatchApp {

    /**
     * Show match statistics.
     * @param matchmaker
     * @param playersPerTeam
     * @param soloPlayers
     */
    public void showSoloMatchWith(Matchmaker matchmaker, int playersPerTeam, List<Player> soloPlayers) {
        MatchmakerImpl matchmakerImpl = (MatchmakerImpl) matchmaker;
        
        String matchmakeMode = "FAIR (no bias)";
        if (matchmakerImpl.getMode() == MatchmakerMode.STRONG) {
            matchmakeMode = String.format("STRONG (%.2f)", matchmakerImpl.getBias());
        } else if (matchmakerImpl.getMode() == MatchmakerMode.WEAK) {
            matchmakeMode = String.format("WEAK (%.2f)", matchmakerImpl.getBias());;
        }

        // print overall stats
        System.out.printf("Player per Team: %d\n", playersPerTeam);
        System.out.printf("Solo Players: %d\n", soloPlayers.size());
        System.out.printf("Matchmake Mode: %s\n", matchmakeMode);

        // subscribe the solo players
        for (Player soloPlayer : soloPlayers) {
            matchmaker.enterMatchmaking(soloPlayer);
        }

        // get match
        Match newMatch = matchmakerImpl.findMatch(playersPerTeam);
        if (newMatch == null) {
            System.err.println("Unable to find any matches.");
            return;
        }

        // print match stats
        System.out.println("Team 1: ");
        int playerIndex = 0;
        for (Player player : newMatch.getTeam1()) {
            System.out.printf("  %d: %s\n", playerIndex, player.toString());
            playerIndex++;
        }

        System.out.println("Team 2: ");
        playerIndex = 0;
        for (Player player : newMatch.getTeam2()) {
            System.out.printf("  %d: %s\n", playerIndex, player.toString());
            playerIndex++;
        }
    }
    public static void main(String [] args) {

        // Do we have arguments provided?
        if (args.length <= 1) {
            // no; show usage and stop.
            System.out.println("Usage: java -cp .:/path/to/json-simple-1.1.1.jar " + 
                                "com.riotgames.interview.hongkong.matchmaking.MatchApp " + 
                                "</path/to/data.json> <players_per_team> {[fair] | strong | weak} " +
                                "{bias_percent: 0.0 - 1.0}");
            return;
        }

        // yes; read the args
        // get the list of players
        JSONParser parser = new JSONParser();
        JSONArray jsonList = null;
        try {
            jsonList = (JSONArray) parser.parse(new FileReader(args[0]));

            // check
            if (jsonList == null || jsonList.size() <= 0) {
                // no players provided
                System.err.println("The \"" + args[0] + "\" file has no players.");
                return;
            }
        } catch(IOException ex) {
            ex.printStackTrace();
        } catch(ParseException ex) {
            ex.printStackTrace();
        }

        // unmarshall each JSONObject into Player and store them in players
        List<Player> players = new ArrayList<Player>();

        for (Object obj : jsonList) {
            JSONObject jsonObj = (JSONObject) obj;

            String name = (String) jsonObj.get("name");
            long wins = (!jsonObj.containsKey("wins")) ? 0 : ((Long) jsonObj.get("wins")).longValue();
            long losses = (!jsonObj.containsKey("losses")) ? 0 : ((Long) jsonObj.get("losses")).longValue();;

            Player aPlayer = new Player(name, wins, losses);
            players.add(aPlayer);
        }

        // get the players per team
        int playersPerTeam = Integer.valueOf(args[1]);

        // get mode; defaults to "fair"
        MatchmakerMode mode = MatchmakerMode.FAIR;
        if (args.length > 2) {
            if (args[2].trim().equalsIgnoreCase("strong")) {
                mode = MatchmakerMode.STRONG;
            } else if (args[2].trim().equalsIgnoreCase("weak")) {
                mode = MatchmakerMode.WEAK;
            }
        }

        // get bias; defaults to 0.7
        double bias = 0.7; 
        if (args.length > 3) {
            bias = Double.parseDouble(args[3]);
        }
        
        // pass to Matchmaker
        Matchmaker matchmaker = new MatchmakerImpl(mode);

        if (bias != ((MatchmakerImpl) matchmaker).getBias()) {
            ((MatchmakerImpl) matchmaker).setBias(bias);
        }
        
        // show the matches
        MatchApp app = new MatchApp();

        app.showSoloMatchWith(matchmaker, playersPerTeam, players);
    }
}