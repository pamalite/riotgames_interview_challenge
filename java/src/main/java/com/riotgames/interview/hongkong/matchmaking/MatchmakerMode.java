package com.riotgames.interview.hongkong.matchmaking;

public enum MatchmakerMode {
    /**
     * Randomly picks from each quartile until number of players are satisfied.
     */
    FAIR, 
    /**
     * Has bias of players with more wins.
     */
    STRONG, 
    /**
     * Has bias of players with more losses.
     */
    WEAK
}
