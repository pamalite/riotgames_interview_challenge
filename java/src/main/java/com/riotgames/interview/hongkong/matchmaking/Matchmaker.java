package com.riotgames.interview.hongkong.matchmaking;

/**
 * The Matchmaker interface to be realized by MatchmakerImpl class.
 */
public interface Matchmaker {

    /**
     * <p>
     * Find a match with the given number of players per team.
     * </p>
     * 
     * @param playersPerTeam
     *            the number of players required in each team
     * @return an appropriate match or null if there is no appropriate match
     */
    Match findMatch(int playersPerTeam);

    /**
     * <p>
     * Add a player for matching.
     * </p>
     * @param player
     */
    void enterMatchmaking(Player player);

}
