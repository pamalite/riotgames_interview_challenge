# Riot Games HKG Studio

## Context

Riot Games HKG Studio coding challenge.

## Problem Statement

Let's pretend that we’ve created a new competitive online team-based game ;)

Players of different skills levels will play the game, so we’ll need to match them in a way that keeps the game fun and fair. We want to match players based on their individual skill level and ensure teams are balanced. We're not expecting each matchup to be perfect (especially not at first), but we want the matchmaking system to eventually get smarter and accurately match players on teams.

Your challenge is to code a comprehensive matchmaking system in any technology you feel comfortable with. Your solution will repeatedly draw a set number of players from a larger pool and match them into teams. Players will enter the matchmaking process as solo participants, so your system should create balanced 3v3, 5v5, etc. teams.

We’ve provided you with sample data in JSON for a pool of 200 players that includes their names, total wins and total losses. We’re happy to toss additional info into the matchmaking mix, so go ahead and invent a new data field and use that in your code if you’re feeling extra ambitious.

## Criteria

An engineer will take whatever you submit and test it, so make sure your solution:

- is documented
- contains the source code
- will compile as submitted
- runs on any machine/OS
- is easy to test ;)

Ideally, we want a flexible system that’ll allow us to edit the matchmaking rules and test out different strategies without extensive engineering efforts.

Last but not least, your solution should be "production-ready", so consider:

- best practices and principles
- build scripts
- tests (unit / integration / coverage)

We've also provided you a Maven/Java project template as a starting point to create your matchmaking system but **feel free to disregard it and propose your own flavor. Use the tech stack of your choice that will allow you to better demonstrate your craft/mastery**.

## My Solution

I have created the following:

- this Git repo for tracking and possible CI purposes
- `src/main` MatchmakerImpl class as the implementation of the Matchmaker interface
- `src/main` MatchmakerMode enum to allow toggling the bias of the matchmaker
- `src/main` MatchApp main class as a test/example usage of MatchmakerImpl class
- `doc/` directory the documents the MatchmakerImpl APIs
- `src/test` MatchmakerImplTest as a simple unit testing using JUnit

I have also updated the provided `pom.xml`. To build:

```
> mvn clean compile package
```

This will:

- remove the `target/` directory and regenerate a new one
- run the JUnit test provided

Inside the `target/` direcoty will also have the executable `hongkong-0.0.1-SNAPSHOT-jar-with-dependencies.jar` file. This is to allow you to either:

- run the MatchApp main class; or
- import to be used with your app

To run the MatchApp main class, use either:

- `mvn clean compile exec:java`; or
   - this needs the `data/` folder with the `sample-data.json` file
   - update the `pom.xml` to point to your data file in JSON if you want to use your own
- `java -jar hongkong-0.0.1-SNAPSHOT-jar-with-dependencies.jar /path/to/sample-data.json playersPerTeam`
   - running this without any arguments will show you full command usage

## Known Issues

1. The MatchmakerImpl.findMatch() will intermittently return unbalanced matches. 
   - This is due to the data provided and the bias and behavior set made the algorithm exhausted all the players. 
   - The workaround now is to stop, and print error, after 5 tries if the match is still not balanced.
